# Cyber Operations Management Tools


> DISCLAIMER
> The views expressed in this document are mine and do not represent the views of any organization, friend, or family that I am affiliated with.

## Summary

Cyber operations often suffer from a lack of communications and consistency. We need a place that acts as a common workspace for event and task management. We need a place to store files such as Indicators of Compromise and internally developed scripts.  We need a place to communicate both formally, such as reporting, and informally, such as chat.

These are key areas needed to be defined for any team to succeed regardless of function.

My goal today is to demonstrate some tools that do some of the work defined above, but maybe not all of it. I will be showing you MCSCop, Dradis CE, theHive, and GitLab CE.

## About Me

You'll have to ask if/when you meet me.

## Category

- Open

My intent is to demonstrate some Cyber Operations Management tools with the goal of getting the conversation started within our Cyber Community. We are missing a high-level platform that can manage our operations, communications, and files.

Given my experience and background I learn new processes through tools. This is because the tools are developed with a certain set of requirements which represent the processes. They also include industry standard terms and jargon that I can research on.

In my effort to learn about Cyber Operations I started researching tools. Not tools that are used for very specific tasks, but for tools that manage how a cyber team operates. As a result I have learned a handful of management tools and have demonstrated them for my peers. This generates discussion about our own operations and processes.

Today, I'd like to generate that discussion here. I will be demonstrating a series of tools and welcoming conversation about processes and workflow.

## Research and Development

### MCSCOP

The tool that sparked my interest in this category of tools was presented to the Gold Team at last year's event.  It is known as *mcscop* and written by psmitty7373.

- mcscop[^1]

Of all the tools covered, this one is the most unique. It offers a common operating picture in the form of a network map.  It also boasts a very functional chat section.

It is in an Alpha state though. The installation and deployment of the software offers very little configuration for an Administrator. If it catches some community support it could become the CPOF of the cyber community.

___

### The Hive with Cortex

The next tool I found in my search is called The Hive. It focuses on Incident Management in the form of Cases and has a range of APIs available to use to include Python and RESTful.  These APIs are most commonly used for Alerts, but can be used in an effort to generate a Report, although this has yet to be proven out.

- The Hive[^2]

Its deployment is much more professional and the application has a completed feel for it. But it is in continuous development. This is good but requires caution when using in Production. You'll want to define a version that works for you and limit deployments of new versions to those that are proved out in your team.

The Hive manages Cases and Tasks as well as allows for the capture of files. It has a mechanism for flagging files as Indicators of Compromise (IOC). It also support Traffic Light Protocol (TLP) for incident sharing. These are used in its integrations with both Cortex and MISP.

The Hive goes hand-in-hand with an analysis automation engine called Cortex. Cortex boasts many plugins to a lot of common analysis tools.

- Cortex[^3]

Cortex recently went through a major change. In earlier versions user management was non-existent and the analysis engine configurations were done in a .yml file. In the latest version you are to create Organizations which house both Users and Analyzers. The configuration of the Analyzers is now done through the gui. Very nice upgrade, but I no longer have the two systems tied together and that integration will not be demonstrated today.

____

### Dradis CE

I was pretty satisfied with The Hive as a Cyber Operations Management tool and my general internet searches were not uncovering any other sound options. It wasn't until I caught wind of the tools on the range for this years event did I learn of Dradis CE.

- Dradis CE[^4]

Dradis CE is an enterprise cyber operations management tool that emphasizes the import of nodes, their relationship to issues, and the export of reports. Some of the more interesting features such as User management and template customizations, are behind a pay wall. This pay wall puts a damper on my research and I can't fully evaulate Dradis CE as a tool.

- [Dradis Framework Editions](https://dradisframework.com/pro/editions.html)

Its text entry is all [Textile](https://www.promptworks.com/textile) based with some special tags for variable mapping. This variable tagging is what is used to generate reports. The template references the tags and the application pulls in information with those tags.

For further reading please see their [support documents](https://dradisframework.com/pro/support/guides/projects/index.html).

___

### GitLab CE

The last tool I'm presenting today is GitLab CE.

GitLab CE is a Developer Operations (DevOps) tool and is not associated with Cyber Operations. But the aspsects of operations are fairly industry agnostic.

- GitLab CE[^5]

GitLab CE houses the common requirements of all Development Operations. It manages work and issues through its backlog. It offers file management through the use of GIT Source Control Management. Its wiki may be a good tool for report generation.

GitLab CE has the potential to host our operations, but this is not why I'm showing it. GitLab CE is mature and well developed. It allows for local deployment and offers OAuth authentication hosting, other apps may authenticate to it.

GitLab CE doesn't have all the functionality of GitLab EE, but it has most everything you would need for a functional environment. But it has zero cyber focus.

- [Feature List](https://about.gitlab.com/features/)

____

## Method/Testing and Redesign

### Deploying MCSCOP

The directions for deploying MCSCOP can be found on psmitty's GitHub page[^6]. At the time of this document the directions were not quite up to date. Below are the steps followed for deploying the instance created for this demonstration.

1. Create a Ubuntu or Debian Virtual Machine.
    - This step is left generic as this function is a common skill.
1. Perform a `git clone` of psmitty's GitHub page[^6].

    ```bash
    git clone https://github.com/psmitty7373/mcscop
    ```

1. Change directory to the newly created *mcscop* directory.

    ```bash
    cd mcscop
    ```

1. As an administrator, execute the `install-cop-apt.sh` command.

    ```bash
    sudo ./install-cop-apt.sh
    ```

1. Follow the on-screen prompts saying *yes* to most options.
    - I'm not sure about the persistance option and have been saying *no* to that.
1. Once the installation is complete use `node` to execute `app.js`.

    ```bash
    node app.js
    ```

1. Navigate to *local ip address*:3000 and login as admin password.

    > Open up your browser to http://mcscop-ip:3000 The default MCSCOP credentials are admin / password.

1. Within the app, create a mission by...
    1. Click the '+' button at the bottom of the table.
    1. Give the mission a name and click enter.

        > No other action will assign the name to the mission and you will not get a link without clicking enter.

    1. Click on the now Linked name of the Mission.
    1. Profit.
1. You have successfully deployed MCSCOP.

____

### Deploying The Hive with Cortex

A prerequisite of the following instructions is the installation of Docker and Docker-Compose on the host system. OS should not matter.

1. Create a dedicated folder for your deployment.
1. Create the following folder structure.

    ```bash
    .
    └── elasticsearch
          └── data
    ```

1. Create a file named `docker-compose.yml` with the following contents...

    ```yml
    version: "2"
    services:
    elasticsearch:
        image: docker.elastic.co/elasticsearch/elasticsearch:5.6.0
        environment:
        - http.host=0.0.0.0
        - transport.host=0.0.0.0
        - xpack.security.enabled=false
        - cluster.name=hive
        - script.inline=true
        - thread_pool.index.queue_size=100000
        - thread_pool.search.queue_size=100000
        - thread_pool.bulk.queue_size=100000
        ulimits:
        nofile:
            soft: 65536
            hard: 65536
        volumes:
        - ./elasticsearch/data:/usr/share/elasticsearch/data
    cortex:
        image: certbdf/cortex:latest
        depends_on:
        - elasticsearch
        ports:
        - "0.0.0.0:9001:9001"
    thehive:
        image: certbdf/thehive:latest
        depends_on:
        - elasticsearch
        - cortex
        ports:
        - "0.0.0.0:9000:9000"
    ```

1. If you are in a Linux system, set the vm.max_map_count to 262144.

    ```bash
    sysctl -w vm.max_map_count=262144
    ```

    > Without this the Elasticsearch Docker container will not start. https://github.com/docker-library/elasticsearch/issues/111

1. Run docker-compose up.

    ```powershell
    docker-compose up
    ```

    > This will look for the docker-compose.yml in your current directory and execute its instructions.

1. Navigate to *localhost*:9000 for the Hive and *localhost*:9001 for Cortex.

### The Hive Administration

This is fairly straight forward and doesn't require any further instruction.

### Cortex Administration

Cortex, not so much...

You'll need to create an Organization, create and Administrator for that Organization, then log in as that Administrator before you're really able to play with the sysytem.

Again...

1. Create an Organization (the default organization does nothing).
1. Create an Administrator for the Organization.
1. Log in as the new Administrator to set up the Organization.

____

### Dradis CE Deployment

A prerequisite of the following instructions is the installation of Docker on the host system. OS should not matter.

1. Create a dedicated folder for your deployment.
1. Create the following folder structure.

    ```bash
    .
    ├── templates
    └── dbdata
    ```

1. Run the following command...

    ```bash
    docker run --name dradis-redis -d redis:alpine
    ```

1. Run the following command...

    ```bash
    docker run --publish 3000:3000 --volume "$(pwd)/dbdata:/dbdata" --link dradis-redis:redis -d evait/dradis-ce
    ```

1. Navigate to *localhost*:3000 in a browser and follow on-screen instructions.

____

### GitLab CE Deployment

A prerequisite of the following instructions is the installation of Docker on the host system. OS should not matter.

Instructions are provided on their webiste: https://docs.GitLab CE.com/omnibus/docker/

1. Create a dedicated folder for your deployment.
1. Create the following folder structure.

    ```bash
    .
    ├── config
    ├── data
    └── logs
    ```
1. Run the following command...

    ```bash
    docker run --detach --hostname GitLab CE.example.com --publish 8443:443 --publish 8080:80 --publish 8022:22 --name GitLab CE --restart always --volume $(pwd)/config:/etc/GitLab CE --volume $(pwd)/logs:/var/log/GitLab CE --volume $(pwd)/data:/var/opt/GitLab CE GitLab CE/GitLab CE-ce:latest
    ```
1. Browse to *localhost*:8080 and follow the on-screen instructions.

____

## Results

Tool|Work Management|File Management|Chat|Reporting|Integration APIs|Community Support|Network Diagram|Event Logging
----|:-------------:|:-------------:|:--:|:-------:|:--------------:|:---------------:|:-------------:|:-----------:
mcscop| |**X**|**X**|**X**| |**X**|**X**|**X**
theHive|**X**|**X**| | |**X**|**X**| |**X**
Dradis CE| |**X**| |**X**| |**X**| |
GitLab CE|**X**|**X**| |**X**|**X**|**X**| |

## Conclusion/Report

At the end of the day there is no tool that covers all of the use cases. I believe we will need to use a combination of tools to achieve our needs. There are many chat servers that can serve as instant communication. There are many file servers that can serve as file management. There are many task management systems as well.

### My Recommendation

If I were to stand up a team, I would use a combination of MCSCOP and The Hive with Cortex. 

What MCSCOP lacks in maturity it makes up for in being the only tool that offers a common operating picture in the form of a network map.

The Hive has many many integration APIs to include Alerts and Analysis activities. It boasts both Case and Task management. Also it allows for file storage and flags such as TLP and IOC.

## Bibliography, References and Acknowledgements

[^1]: https://github.com/psmitty7373/mcscop

[^2]: http://thehive-project.org/

[^3]: https://github.com/TheHive-Project/CortexDocs

[^4]: https://dradisframework.com/ce/

[^5]: https://about.gitlab.com/

[^6]: https://github.com/psmitty7373/mcscop