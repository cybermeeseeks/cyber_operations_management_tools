docker run --detach \
  --hostname gitlab.example.com \
  --publish 8443:443 --publish 8080:80 --publish 8022:22 \
  --name gitlab \
  --restart always \
  --volume $(pwd)/config:/etc/gitlab \
  --volume $(pwd)/logs:/var/log/gitlab \
  --volume $(pwd)/data:/var/opt/gitlab \
  gitlab/gitlab-ce:latest
